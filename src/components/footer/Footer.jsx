import React from 'react'
import { useCurrentDate } from "@kundinos/react-hooks";
import { Foot } from './style.js';

export function Footer() {
  const currentDate = useCurrentDate();

  const month = currentDate.getMonth();
  const date = currentDate.getDate();
  const fullYear = currentDate.getFullYear();

  return (
    <Foot>
      <div className="footer__wrapper">
        <p>{`Текущая дата: ${month}.${date}.${fullYear}`}</p>
      </div>
    </Foot>
  )
}

// export default Footer
