
import React, { useEffect, useState } from 'react'
import Main from '../main/Main'
import Sidebar from '../sidebar/Sidebar'
import './Centered.css'

function Centered() {

  useEffect(() => {
    console.log('render Centered');
  }, [])

  return (
    <div className="wrapper">
      <Main />
      <Sidebar />
    </div>
  )
}

export default Centered
