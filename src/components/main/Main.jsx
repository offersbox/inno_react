import React, { useEffect, useState } from 'react'
import MemoryConfig from '../memory-config/MemoryConfig'
import ReviewList from '../reviews/ReviewList'
import './Main.css'

function Main() {


  useEffect(() => {
    console.log('render Main')
  }, [])

  return (
    <div className="centered">
      <MemoryConfig />
      <ReviewList />
    </div>
  )
}

export default Main
