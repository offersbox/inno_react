import React from 'react'

function ReviewStars({ score }) {

  return (
    <div className="star-rating">
      {[...Array(5)].map((el, index) => {
        if (index < score) {
          return (
            <img src={'static/img/Star-red.png'} />
          );
        }

        return (
          <img src={'static/img/Star-grey.png'} />
        )
      })}
    </div>
  )
}

export default ReviewStars
