import React, { useState } from 'react'
import { buttonsValues, buttonsObjects } from '../../mocks/data'
import './MemoryConfig.css'

function MemoryConfig() {
  const [values, setValues] = useState(buttonsObjects);
  const [selectedIdx, setSelectedIdx] = useState(0);

  const handleClick = (index) => {
    setSelectedIdx(index)
  }

  return (
    <div className="memory-config">
      <div className="memory-config__buttons">
        {values.map(({ id, name }, idx) => (
          <button
            className={
              idx === selectedIdx ? 'btn btn_selected' : 'btn'
            }
            onClick={() => handleClick(idx)}
            key={id}
          >
            {name}
          </button>
        ))}
      </div>
    </div>
  )
}

export default MemoryConfig
