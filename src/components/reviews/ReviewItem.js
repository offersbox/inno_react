import React from 'react'
import ReviewStars from '../review-stars/ReviewStars';

// function ReviewItem(props) {
function ReviewItem({ review, counter, setCounter }) {

  const { author, score, avatar, usageExperience, advantage, disadvantages } = review;

  return (
    <>
      <div className="review">
        <img src={avatar}
          className="review__photo" alt="фото автора отзыва" />
        <div className="review__content">
          <h4 className="review__name">{author}</h4>
          {/* <img src="./img_new/Rating 5 stars.png"
            className="review__rating" alt="Оценка 5 звезд" /> */}
          <ReviewStars score={score} />

          <div>Счетчик: {counter}</div>
          <button onClick={() => setCounter(prev => prev + 1)}>
            Увеличить счетчик
          </button>

          <div className="review__parameters">
            <div className="review__parameter"><strong>Опыт
              использования:</strong>
              {usageExperience}
            </div>
            <div className="review__parameter">
              <div>
                <strong>Достоинства:</strong>
              </div>
              {advantage}
            </div>
            <div className="review__parameter">
              <div>
                <strong>Недостатки:</strong>
              </div>
              {disadvantages}
            </div>
          </div>
        </div>
      </div>
      <div className="separator"></div>
    </>
  )
}

export default ReviewItem
