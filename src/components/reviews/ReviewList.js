import React, { useState } from 'react'
import { reviewsData } from '../../mocks/data';
import ReviewItem from './ReviewItem';
import './Reviews.css'

function ReviewList() {
  const [state, setState] = useState(reviewsData);
  const [counter, setCounter] = useState(0);

  return (
    <section className="review-section">
      <div className="review-section__header">
        <div className="review-section__text">
          <h3 className="review-section__title">Отзывы</h3>
          <span className="review-section__count">425</span>
        </div>
      </div>

      <div className="review-section__list">
        {state && state.map(review => (
          <ReviewItem
            review={review}
            counter={counter}
            setCounter={setCounter}
          />
        ))}
      </div>

    </section>)
}

export default ReviewList
