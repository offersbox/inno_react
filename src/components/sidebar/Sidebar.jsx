import React, { memo, useEffect } from 'react'
import './Sidebar.css'

function Sidebar() {
  useEffect(() => {
    console.log('render Sidebar')
  }, [])

  return (
    <div className="sidebar">
      <div className="sidebar__wrapper">
        Sidebar
      </div>
    </div>
  )
}

export default memo(Sidebar)
