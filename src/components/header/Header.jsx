import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

function Header() {
  useEffect(() => {
    console.log('render Header')
  }, [])

  return (
    <div className="page__header">
      <div className="header__wrapper">
        <nav>
        <ul>
          <li>
            <Link to="/">Main</Link>
          </li>
          <li>
            <Link to="/product">Product</Link>
          </li>
        </ul>
        </nav>
      </div>
    </div>
  )
}

export default Header
