"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reviewsData = exports.buttonsObjects = exports.buttonsValues = void 0;
var buttonsValues = ['128 Гб', '256 Гб', '512 Гб'];
exports.buttonsValues = buttonsValues;
var buttonsObjects = [{
  id: 1,
  name: '128 Гб'
}, {
  id: 2,
  name: '256 Гб'
}, {
  id: 3,
  name: '512 Гб'
}];
exports.buttonsObjects = buttonsObjects;
var reviewsData = [{
  author: 'Марк Г.',
  score: 5,
  avatar: 'static/img/Author-photo-1.png',
  usageExperience: 'менее месяца',
  advantage: 'это мой первый айфон после после огромного количества телефонов на андроиде. всёплавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.',
  disadvantages: 'к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное'
}, {
  author: 'Валерий Коваленко',
  avatar: 'static/img/Author-photo-2.png',
  score: 4,
  usageExperience: 'менее месяца',
  advantage: 'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
  disadvantages: 'Плохая ремонтопригодность'
}, {
  author: 'Ivanov Ivan',
  avatar: '../img/Author-photo-2.png',
  score: 3,
  usageExperience: 'менее месяца',
  advantage: 'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
  disadvantages: 'Плохая ремонтопригодность'
}];
exports.reviewsData = reviewsData;