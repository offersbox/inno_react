import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import { Footer } from './components/footer/Footer';
import Header from './components/header/Header';
import Main from './pages/Main';
import Product from './pages/Product';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
          !!!!
        <Header />

        <Routes>
          <Route path="/" element={<Main />} />
          <Route path='/product' element={<Product />} />
        </Routes>
      </BrowserRouter>

      <Footer />
    </div>
  );
}

export default App;
